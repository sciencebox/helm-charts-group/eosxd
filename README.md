## ---Deprecated---

Please check [eos-charts](https://gitlab.cern.ch/eos/eos-charts/-/tree/master/eosxd) repo instead.

-----

# eosxd

The Helm chart for the EOS FuseX mount
